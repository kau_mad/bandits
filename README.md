# Bandits
Python library for Multi-Armed Bandits

Forked from Bandits (https://github.com/bgalbraith/bandits). Extended by introducing
algorithms for bandits in metric spaces.

Implements the following algorithms:
* Epsilon-Greedy
* UCB1
* Softmax
* Thompson Sampling (Bayesian)
  * Bernoulli, Binomial <=> Beta Distributions
* Bandits in metric space
  * LinRel [1]
  * GP-UCB [2]
  * iKNN-UCB (Proposed method)

[1] Auer, Peter. "Using confidence bounds for exploitation-exploration trade-offs." Journal of Machine Learning Research 3.Nov (2002): 397-422.

[2] Srinivas, Niranjan, et al. "Gaussian process optimization in the bandit setting: No regret and experimental design." arXiv preprint arXiv:0912.3995 (2009).

## Installing
Requires a Python 3.x environment.

Dependencies:
* Numpy
* PYMC3
* Theano
* Matplotlib
* seaborn

Running this command in the bandits root directory will install the bandits library
with all its dependencies.

``$python setup.py install``

## Examples
* [Bayesian Belief](https://github.com/bgalbraith/bandits/tree/master/notebooks/Stochastic%20Bandits%20-%20Bayesian%20Belief.ipynb)
* [Value Estimation Methods](https://github.com/bgalbraith/bandits/tree/master/notebooks/Stochastic%20Bandits%20-%20Value%20Estimation.ipynb)
* [Preference Estimation](https://github.com/bgalbraith/bandits/tree/master/notebooks/Stochastic%20Bandits%20-%20Preference%20Estimation.ipynb)
