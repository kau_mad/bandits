import numpy as np


class Response:
    def __init__(self, reward, optimal, active_arms=None, score=-1):
        self.reward = reward
        self.optimal = optimal
        self.active_arms = active_arms

        if score >= 0:
            self.score = score
        else:
            self.score = reward


class BanditExecuter:
    """
    Executer class is used to implement the action to be
     called when an arm (action) is pulled by the Bandit.
     The action is implemented in the run method.
    """
    def run(self, action):
        """
        This method is called when an action (arm) is
        being executed.
        :param action:
        :return: reward for playing the action
        """
        return 0

    def get_current_state(self):
        return NotImplementedError
