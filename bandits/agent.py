import numpy as np
import pymc3 as pm


class Agent(object):
    """
    An Agent is able to take one of a set of actions at each time step. The
    action is chosen using a strategy based on the history of prior actions
    and outcome observations.
    """
    def __init__(self, bandit, policy, prior=0, gamma=None):
        self.policy = policy
        self.k = bandit.k
        self.prior = prior
        self.gamma = gamma
        self._value_estimates = prior*np.ones(self.k)
        self.action_attempts = np.zeros(self.k)
        self.t = 0
        self.last_action = None
        self._arms = np.arange(0, self.k)
        self.active_arms = np.ones(self.k, dtype=bool)

    def __str__(self):
        return 'f/{}'.format(str(self.policy))

    def reset(self):
        """
        Resets the agent's memory to an initial state.
        """
        self._value_estimates[:] = self.prior
        self.action_attempts[:] = 0
        self.last_action = None
        self.t = 0
        self.active_arms = np.ones(self.k, dtype=bool)

    def choose(self):
        action = self.policy.choose(self)
        self.last_action = action
        return action

    def observe(self, reward):
        self.action_attempts[self.last_action] += 1

        if self.gamma is None:
            g = 1 / self.action_attempts[self.last_action]
        else:
            g = self.gamma
        q = self._value_estimates[self.last_action]

        self._value_estimates[self.last_action] += g*(reward - q)
        self.t += 1

    @property
    def value_estimates(self):
        return self._value_estimates

    @property
    def arms(self):
        return self._arms[self.active_arms]


class GradientAgent(Agent):
    """
    The Gradient Agent learns the relative difference between actions instead of
    determining estimates of reward values. It effectively learns a preference
    for one action over another.
    """
    def __init__(self, bandit, policy, prior=0, alpha=0.1, baseline=True):
        super(GradientAgent, self).__init__(bandit, policy, prior)
        self.alpha = alpha
        self.baseline = baseline
        self.average_reward = 0

    def __str__(self):
        return 'g/\u03B1={}, bl={}'.format(self.alpha, self.baseline)

    def observe(self, reward):
        self.action_attempts[self.last_action] += 1

        if self.baseline:
            diff = reward - self.average_reward
            self.average_reward += 1/np.sum(self.action_attempts) * diff

        pi = np.exp(self.value_estimates) / np.sum(np.exp(self.value_estimates))

        ht = self.value_estimates[self.last_action]
        ht += self.alpha*(reward - self.average_reward)*(1-pi[self.last_action])
        self._value_estimates -= self.alpha*(reward - self.average_reward)*pi
        self._value_estimates[self.last_action] = ht
        self.t += 1

    def reset(self):
        super(GradientAgent, self).reset()
        self.average_reward = 0


class BetaAgent(Agent):
    """
    The Beta Agent is a Bayesian approach to a bandit problem with a Bernoulli
     or Binomial likelihood, as these distributions have a Beta distribution as
     a conjugate prior.
    """
    def __init__(self, bandit, policy, ts=True):
        super(BetaAgent, self).__init__(bandit, policy)
        self.n = bandit.n
        self.ts = ts
        self.model = pm.Model()
        with self.model:
            self._prior = pm.Beta('prior', alpha=np.ones(self.k),
                                  beta=np.ones(self.k), shape=(1, self.k),
                                  transform=None)
        self._value_estimates = np.zeros(self.k)

    def __str__(self):
        if self.ts:
            return 'b/TS'
        else:
            return 'b/{}'.format(str(self.policy))

    def reset(self):
        super(BetaAgent, self).reset()
        self._prior.distribution.alpha = np.ones(self.k)
        self._prior.distribution.beta = np.ones(self.k)

    def observe(self, reward):
        self.action_attempts[self.last_action] += 1

        self.alpha[self.last_action] += reward
        self.beta[self.last_action] += self.n - reward

        if self.ts:
            self._value_estimates = self._prior.random()
        else:
            self._value_estimates = self.alpha / (self.alpha + self.beta)
        self.t += 1

    @property
    def alpha(self):
        return self._prior.distribution.alpha

    @property
    def beta(self):
        return self._prior.distribution.beta


class BayesianLinearAgent(Agent):
    """
    The Bayesian Agent is an implementation of Thompson sampling.
    """
    def __init__(self, bandit, policy=None,
                 epsilon=0.01, delta=0.01, has_bias=False):
        # super(BayesianLinearAgent, self).__init__(bandit, policy)
        self.d = bandit.d
        self._has_bias = has_bias
        self.__init_params()
        self.executer = bandit.executer
        r = 1.
        self._v2 = np.square(r)*(24/epsilon)*self.d*np.log(1./delta)
        self.t = 0

    def __init_params(self):
        self.B = np.identity(self.d, np.float)
        self.B_inv = np.identity(self.d, np.float)
        self.mu = np.zeros(self.d, np.float)
        self._f = np.zeros(self.d, np.float)

    def reset(self):
        self.__init_params()
        self.t = 0

    def __str__(self):
        return 'b/TS'

    def choose(self):
        mu_hat = np.random.multivariate_normal(self.mu, self._v2*self.B_inv)
        # TODO vectorize these
        y = np.array([np.dot(self.executer.get_context(arm).reshape(-1), mu_hat)
                      for arm in self.executer.arms])
        if self._has_bias:
            bias = np.array([self.executer.get_bias(arm) for arm in self.executer.arms])
            y = y - bias
        best_arm = self.executer.arms[np.argmax(y)]
        self.last_action = self.executer.get_context(best_arm)
        return best_arm

    def observe(self, reward):
        bxb = np.asmatrix(self.last_action)
        self.B += np.matmul(bxb.T, bxb)
        self._f += reward * self.last_action.reshape(-1)
        self.B_inv = np.linalg.inv(self.B)
        self.mu = np.matmul(self.B_inv, self._f.T)
        self.t += 1


class MetricAgent(Agent):
    """
    MetricAgent is to be used with Regression models implementing
    Scikit-Learn API. The model learns a pattern from observations
    (arm, reward pairs). The value estimate of each arm is
    predicted by the specified model.
    """

    def __init__(self, bandit, policy, model, partial_fit=False):
        self.bandit = bandit
        self.d = bandit.d
        self.policy = policy
        self._model = model
        self._x = None
        self.X = []
        self.Y = []
        self.last_action_context = None
        self.t = 0
        self._has_bias = True #move this to executer
        self._supports_partial_fit = partial_fit

    def reset(self):
        self.X = []
        self.Y = []
        self.t = 0

    def choose(self):
        action = self.policy.choose(self)
        self.last_action = action
        self.last_action_context = self.bandit.executer.get_context(action)
        return action

    def observe(self, reward):
        self.X.append(self.last_action_context.reshape(-1))
        self.Y.append(reward)
        if len(self.Y) >= self.policy.n0:
            if not self._supports_partial_fit or not self._model.fitted:
                self._model.fit(self.X, self.Y)
            else:
                self._model.partial_fit(self.last_action_context.reshape(1, -1), reward)
        self.t += 1

    @property
    def value_estimates(self):
        x = np.vstack([self.executer.get_context(arm).transpose() for arm in self.executer.arms])
        self._x = x
        # scores = [self._model.predict(point) for point in x]
        scores = self._model.predict(x)
        if self._has_bias:
            bias = np.array([self.executer.get_bias(arm) for arm in self.arms])
            scores -= bias
        return scores

    @property
    def executer(self):
        return self.bandit.executer

    @property
    def arms(self):
        return self.executer.arms

    @property
    def contexts(self):
        return self._x

    @property
    def model(self):
        return self._model

    def __str__(self):
        return self._model.__str__()


class LinRelAgent(Agent):
    def __init__(self, bandit, policy, kernel=None, log_normal=False):
        self.bandit = bandit
        self.d = bandit.d
        self.policy = policy
        self._A = np.identity(self.d, dtype=float)
        self._invA = np.identity(self.d, dtype=float)
        self._b = np.zeros((self.d, 1), dtype=float)
        self.theta = np.random.rand(self.d, 1)
        self.t = 0
        self.last_action_context = None
        self._log_normal = log_normal
        self._x = None

    def reset(self):
        self._A = np.identity(self.d, dtype=float)
        self._invA = np.identity(self.d, dtype=float)
        self._b = np.zeros((self.d, 1), dtype=float)
        self.theta = np.random.rand(self.d, 1)
        self.t = 0

    def choose(self):
        action = self.policy.choose(self)
        self.last_action = action
        self.last_action_context = self.bandit.executer.get_context(action)
        return action

    def observe(self, reward):
        if self._log_normal and reward > 0:
            reward = np.log(reward)
        self._A += self.last_action_context.dot(self.last_action_context.T)
        self._b += reward * self.last_action_context
        self._invA = np.linalg.inv(self._A)
        self.theta = self._invA.dot(self._b)
        self.t += 1

    @property
    def value_estimates(self):
        x = np.array([self.executer.get_context(arm).reshape(-1) for arm in self.executer.arms])
        self._x = x
        scores = (x.dot(self.theta)).reshape(-1)
        return scores

    @property
    def executer(self):
        return self.bandit.executer

    @property
    def arms(self):
        return self.executer.arms

    @property
    def contexts(self):
        return self._x

    def __str__(self):
        return 'LIN'


class Exp3Agent(Agent):
    """
    Agent class for EXP3 adversarial bandit algorithm
    """

    def __init__(self, bandit, policy, gamma, eta=2):
        self.policy = policy
        self.gamma = gamma
        self.eta = eta
        self.k = bandit.k
        self.weights = np.ones(self.k)
        self.last_action = None
        self.active_arms = np.ones(self.k, dtype=bool)
        self.arm_probs = np.zeros(self.k, dtype=float)
        self.t = 0

    def reset(self):
        self.weights[:] = 1.0
        self.active_arms = np.ones(self.k, dtype=bool)

    def observe(self, reward):
        estimated_reward = reward/(self.arm_probs[self.last_action] + self.gamma)
        c = np.exp(self.eta * estimated_reward / self.k)
        self.weights[self.last_action] = self.weights[self.last_action] * c