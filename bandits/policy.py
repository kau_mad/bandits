import numpy as np


class Policy(object):
    """
    A policy prescribes an action to be taken based on the memory of an agent.
    """

    def __str__(self):
        return 'generic policy'

    def choose(self, agent):
        return 0


class EpsilonGreedyPolicy(Policy):
    """
    The Epsilon-Greedy policy will choose a random action with probability
    epsilon and take the best apparent approach with probability 1-epsilon. If
    multiple actions are tied for best choice, then a random action from that
    subset is selected.
    """

    def __init__(self, epsilon, n0=20):
        self.epsilon = epsilon
        self.n0 = n0

    def __str__(self):
        return '\u03B5-greedy (\u03B5={})'.format(self.epsilon)

    def choose(self, agent):
        if agent.t < self.n0 or np.random.random() < self.epsilon:
            return np.random.choice(agent.arms)
        else:
            action = np.nanargmax(agent.value_estimates)
            check = np.where(agent.value_estimates == action)[0]
            if len(check) == 0:
                return agent.arms[action]
            else:
                return agent.arms[np.random.choice(check)]


class GreedyPolicy(EpsilonGreedyPolicy):
    """
    The Greedy policy only takes the best apparent action, with ties broken by
    random selection. This can be seen as a special case of EpsilonGreedy where
    epsilon = 0 i.e. always exploit.
    """

    def __init__(self):
        super(GreedyPolicy, self).__init__(0)

    def __str__(self):
        return 'greedy'


class RandomPolicy(EpsilonGreedyPolicy):
    """
    The Random policy randomly selects from all available actions with no
    consideration to which is apparently best. This can be seen as a special
    case of EpsilonGreedy where epsilon = 1 i.e. always explore.
    """

    def __init__(self):
        super(RandomPolicy, self).__init__(1)

    def __str__(self):
        return 'random'


class UCBPolicy(Policy):
    """
    The Upper Confidence Bound algorithm (UCB1). It applies an exploration
    factor to the expected value of each arm which can influence a greedy
    selection strategy to more intelligently explore less confident options.
    """

    def __init__(self, c):
        self.c = c

    def __str__(self):
        return 'UCB (c={})'.format(self.c)

    def choose(self, agent):
        if np.any(agent.action_attempts == 0):
            matches = np.where((agent.action_attempts == 0) & agent.active_arms)[0]
            if np.size(matches) > 0:
                return matches[0]

        exploration = np.empty_like(agent.value_estimates, dtype=float)
        for idx, num_attempts in enumerate(agent.action_attempts):
            if agent.active_arms[idx]:
                exploration[idx] = np.log(agent.t + 1) / num_attempts
            else:
                exploration[idx] = np.nan
        exploration = np.power(exploration, 1 / self.c)

        q = agent.value_estimates + exploration

        action = np.nanargmax(q)
        check = np.where(q == action)[0]
        if len(check) == 0:
            return action
        else:
            return np.random.choice(check)


class Exp3Policy(Policy):
    """
    Exponential weighted algorithm for adversarial bandit problem
    """

    def __str__(self):
        return 'EXP3'

    def choose(self, agent):
        """
        draws an action randomly according to the probability
        distribution calculated using weights of the actions
        :param agent:
        :return: action
        """
        num_active_arms = np.sum(agent.active_arms)
        multiplier = (1.0 - agent.gamma) / np.sum(agent.weights[agent.active_arms])
        agent.arm_probs[agent.active_arms] = agent.weights[agent.active_arms] * multiplier \
                                             + agent.gamma / num_active_arms
        agent.arm_probs[[not _ for _ in agent.active_arms]] = 0.
        return np.random.choice(np.arange(agent.k), p=agent.arm_probs)


class GpUcbPolicy(Policy):
    """
    Gaussian Process - UCB policy.
    """
    def __init__(self, training_rounds=20):
        self._has_bias = True
        self._training_rounds = training_rounds

    def __str__(self):
        return 'GP-UCB'

    def choose(self, agent):
        if len(agent.X) < self._training_rounds:
            return np.random.choice(agent.executer.arms)

        x = np.array([agent.executer.get_context(arm).reshape(-1) for arm in agent.executer.arms])
        mu, sigma = agent._model.predict(x)

        if self._has_bias:
            bias = np.array([agent.executer.get_bias(arm) for arm in agent.executer.arms])

        scores = mu + agent.beta * sigma - bias

        best_arm = agent.executer.arms[np.argmax(scores)]
        return best_arm


class LinUCBPolicy(Policy):
    """
    The LinUCB policy assumes a linear relationship between the arm representation
    and the reward each arm will result in. This policy is employed by LinRelAgent.
    """

    def __init__(self, alpha, log_normal=False):
        self.alpha = alpha
        self._log_normal = log_normal
        self._has_bias = True

    def __str__(self):
        return 'UCB (\u03B1 = %.1f)' % self.alpha

    def choose(self, agent):
        value_estimates = agent.value_estimates
        rlogt = self.alpha * np.sqrt(agent.d) * np.log(agent.t + 1.)
        x = agent.contexts
        conf_ball = [rlogt * np.sqrt((x[i, :].dot(agent._invA)).dot(x[i, :])) for i in range(x.shape[0])]
        value_estimates += conf_ball
        if self._log_normal:
            value_estimates = [np.exp(s) for s in value_estimates]
        if self._has_bias:
            bias = np.array([agent.executer.get_bias(arm) for arm in agent.arms])
            value_estimates -= bias
        best_arm = agent.arms[np.argmax(value_estimates)]
        return best_arm


class KNNUCBPolicy(Policy):
    """
    The proposed iKNN-UCB algorithm.
    Confidence bound = expected reward (from kNN regression) + exploration term.
    Exploration term for a point x is computed as the average distance to
    k-neighbors from x.
    """

    def __init__(self, alpha, delta=0.5, k2=10, n0=20):
        self.alpha = alpha
        self.delta = delta
        self._has_bias = True
        self._k2 = k2
        self._n0 = n0

    def __str__(self):
        return 'UCB (\u03B1 = %.1f)' % self.alpha

    def choose(self, agent):
        if agent.t < self._n0:
            return np.random.choice(agent.arms)

        value_estimates = agent.value_estimates
        dist, neigh_ind = agent.model.kneighbors(agent.contexts, n_neighbors=self._k2)
        # n = np.sum(np.exp(-1*np.square(dist)), axis=1)
        # pos_logn = np.log(n)
        # pos_logn[pos_logn < 0] = 0.
        # sigma = np.sqrt(pos_logn*self.delta) * np.power(n, -1/(2+agent.d))
        sigma = np.sum(dist)
        ucb = value_estimates + self.alpha * sigma
        best_arm_index = np.argmax(ucb)

        return agent.arms[best_arm_index]

    @property
    def n0(self):
        return self._n0


class SoftmaxPolicy(Policy):
    """
    The Softmax policy converts the estimated arm rewards into probabilities
    then randomly samples from the resultant distribution. This policy is
    primarily employed by the Gradient Agent for learning relative preferences.
    """

    def __str__(self):
        return 'SM'

    def choose(self, agent):
        a = agent.value_estimates[agent.active_arms]
        pi = np.exp(a) / np.sum(np.exp(a))
        cdf = np.cumsum(pi)
        s = np.random.random()
        return np.where(s < cdf)[0][0]
